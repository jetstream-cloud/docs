# Using Manila Shares in Horizon

!!! info "Share names and Access Rule Names must be unique!"

    While shares and access rules to shares belong to specific allocations, the namespace is for the entire cloud. You will have to have a unique share name and unique rule names. We suggest using a descriptive name and using a variation of the name for the access rule(s).

---

## Creating a Manila Share in Horizon

1. Click on:  `Project`  → `Share` → `Shares` → `+ Create Share`

![Screenshot of the Horizon "Shares" page, highlighting the "Share" section and the "+ Create Share" button](../../images/JS2-manila1.png){.round-and-shadow}

2. `Create share` with the following settings:

- **Share Name**: a descriptive and **globally unique** name of your choosing
- **Share Protocol**: CephFS
- **Size**: the size of your manila share in gigabytes
- **Share Type**: `cephnfsnativetype`

![Screenshot of the "Create Share" modal](../../images/JS2-manila2.png){.round-and-shadow}

---

## Editing Access Rules

A Manila Share cannot be accessed/mounted without at least one "Access Rule." Once your Manila Share is available, you can select `Manage Rules` and `Add Rule`:

![Screenshot showing the share actions dropdown expanded and the "Manage Rules" button circled in red](../../images/JS2-manila3.png){.round-and-shadow}

- **Access Type**: cephx
- **Access Level**: read-write
- **Access To**: an descriptive and **globally unique** rule name

![Screnshot of the "Add Rule" modal](../../images/JS2-manila4.png){.round-and-shadow}

!!! warning "Reminder about unique names"
    
    In the example above, the `accessTo` name is `manilashare`. ***The name assigned must be globally unique!*** If you use a name that is already in use you will see an error state. To help avoid accidental overlap, we recommend prefixing your name with something unique such as your ACCESS username or your allocation number. For example, instead of using just `seed-research`, use `<ALLOCATION>-seed-research`.

If you now go back to the share page (`Project`/`Share`/`Shares`) and click on the share you created, you should see your share's details.

Important things to note here are :

- **Path**: `IPs`:`ports` followed by volume path (`/volume/_no-group/...`)
- **Access Key**: the secret credential that allows mounting of your Manila Share

![Screenshot of a "Share Details" page with the "Path" and "Access Key" rows circled in red](../../images/JS2-manila5.png){.round-and-shadow}

---




### Using a Manila Share on an Instance

This is the same whether you're using Horizon or the CLI. Please refer to [Configuring a VM to use Manila Shares](../../general/manilaVM.md).