# Logging into horizon

---

#### 1. Navigate

* Navigate to [https://js2.jetstream-cloud.org](https://js2.jetstream-cloud.org)
* Make sure it says "ACCESS CILogon" in the **Authenticate Using** box.

![Screenshot of the "Authenticate using" dropdown box on the Horizon login page](../../images/JS2-Horizon-Login-Screen.png)

---

#### 2. CILogon Authentication

* You will be directed to a CILogon page to permit authorization the first time and any time your are using a new browser or your credentials go stale.
* If you have linked institutional, Google, Orcid, or other credentials, you'll be able to use those to authenticate.

We know ***ACCESS-CI*** credentials work correctly so we will show that in our example and recommend using those unless you are 100% sure you have linked credentials PREVIOUSLY in CILogon

!!! note "Other intstitution credentials"

     Please note that other CILogon credentials (i.e. for your home institution or Google) must be set up outside of this process. Follow these [Linking instructions](https://identity.access-ci.org/id-linking){target=_blank} to link your home institution or other organization (e.g. Google, ORCID, etc) credentials.

![Screenshot with "ACCESS CI" selected for the "Select an Identity Provider" dropdown](../../images/JS2-CILogin-Screen.png)

---

#### 3. ACCESS credentials

* The next page should be the login screen for your credentials. We're showing the ACCESS login screen as our example.

![Screenshot of the ACCESS/CILogon login screen](../../images/JS2-CILogin-Auth-Screen.png)

---

#### 4. Multi Factor Authentication

* If you're using multi-factor authentication with your credentials as ACCESS does, you'll likely get a Duo or an Authenticator screen here.

![Screenshot of an ACCESS Duo MFA prompt](../../images/JS2-Access-Duo-Screen.png)

---

#### 5. Horizon Dashboard

* You should be at the Horizon Dashboard home now.

![Screenshot of the Horizon home page](../../images/JS2-Horizon-Home.png)

---

#### 6. Project

* If you are on multiple ACCESS allocations, you'll want to verify you're using the correct one and change to the correct one if you are not.<br /><br />
You do that by clicking at the top left next to the Jetstream2 logo where it has <span style="background: #e2e5e7; padding: 5px;">ACCESS &#x2022; AAA000000 &#x2022; IU</span>. 
That will show allocations under "Projects".

![Screenshot highlighting the project selection dropdown on Horizon](../../images/JS2-Horizon-Change-Projects.png)